package tom.reactor.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ZHUFEIFEI
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MsgEvent {
    private Long id;
    private String content;
}
