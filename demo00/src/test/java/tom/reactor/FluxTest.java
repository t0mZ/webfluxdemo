package tom.reactor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

/**
 *
 * 响应式编程练习
 *
 */
public class FluxTest {

    private List<String> words;

    @BeforeEach
    public void before() {
        this.words = Arrays.asList("the", "quick", "brown", "fox", "jumped", "over", "the", "lazy", "dog");
    }

    @Test
    public void testFlux() {
        Flux<String> fewWords = Flux.just("hello", "world");
        Flux<String> manyWords = Flux.fromIterable(this.words);

        fewWords.subscribe(System.out::println);

        System.out.println();

        manyWords.subscribe(System.out::println);
    }

    @Test
    public void findingMissingLetter() {
        //输入流, 统计字母
        Flux<String> letters = Flux.fromIterable(this.words)
                //将流中的数据展开并返回另一个flux流
                .flatMap(word -> Flux.fromArray(word.split("")))
                //流中的数据去重
                .distinct()
                //排序
                .sort()
                //聚合, 第一个参数是一个索引器，创建自增流。该方法是聚合原数据流和新的数据流(range生成一个数据流)形成一个聚合后的数据流
                .zipWith(Flux.range(1, Integer.MAX_VALUE), (string, seq) -> String.format("%2d. %s", seq, string));

        letters.subscribe(System.out::println);
    }

    @Test
    public void restoreMissingLetter() {
        //输入流, 上一个例子中缺少字母s,下面填充缺失字母
        Flux<String> letters = Flux.fromIterable(this.words)
                //将流中的数据展开并返回另一个flux流
                .flatMap(word -> Flux.fromArray(word.split("")))
                //将原流与指定流连接
                .concatWith(Mono.just("s"))
                //流中的数据去重
                .distinct()
                //排序
                .sort()
                //聚合, 第一个参数是一个索引器，创建自增流。该方法是聚合原数据流和新的数据流(range生成一个数据流)形成一个聚合后的数据流
                .zipWith(Flux.range(1, Integer.MAX_VALUE), (string, seq) -> String.format("%2d. %s", seq, string));

        letters.subscribe(System.out::println);
    }

    /**
     * 当主线程订阅时，由于子线程延迟，主线程订阅完毕会马上退出，收不到发布的消息
     */
    @Test
    public void shortCircuit() {
        Flux<String> hello = Mono.just("hello").concatWith(Mono.just("world"))
                //只取第一个元素
                .take(1)
                //订阅延迟500毫秒
                .delaySubscription(Duration.ofMillis(500));
        //订阅后立即返回，由于消息延迟，消费不到子线程数据
//        hello.subscribe(System.out::println);

        //阻塞直到流可用
        hello.toStream().forEach(System.out::println);
    }

    @Test
    public void testFluxInterval() {
        //从0开始每隔1秒递增一个打印，转化为字符串再次打印出来
        //一直执行到最后一个元素，这里就是无限执行
        Flux.interval(Duration.ofSeconds(1L))
                .log()
                .map(n -> String.format("seconds : %d", n))
                .log()
                .blockLast();
    }


}
