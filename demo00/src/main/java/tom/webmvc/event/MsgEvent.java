package tom.webmvc.event;

import org.springframework.context.ApplicationEvent;

/**
 *
 * @author ZHUFEIFEI
 */
public class MsgEvent extends ApplicationEvent {

    public MsgEvent(Object source) {
        super(source);
    }
}
