package tom.webmvc.event.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.SmartApplicationListener;
import org.springframework.stereotype.Component;
import tom.webmvc.event.MsgEvent;
import tom.webmvc.service.SseService;

/**
 * msg 事件监听
 * @author ZHUFEIFEI
 */
@Slf4j
@Component
public class MsgEventListener implements ApplicationListener<MsgEvent> {

    @Autowired
    private SseService sseService;

    @Override
    public void onApplicationEvent(MsgEvent msgEvent) {
        this.sseService.sendMsg(msgEvent.getSource().toString());
    }
}
