package tom.webmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * spring-mvc 的sse 服务器发送事件 例子
 * @author ZHUFEIFEI
 */
@SpringBootApplication
public class WebMvcSseDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebMvcSseDemoApplication.class, args);
    }
}
