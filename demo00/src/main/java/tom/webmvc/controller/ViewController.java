package tom.webmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author ZHUFEIFEI
 */
@Controller
@RequestMapping("/view")
public class ViewController {

    @GetMapping("/sse")
    public String sse(Model model) {
        model.addAttribute("title", "Server Send Event");
        return "sse";
    }
}
