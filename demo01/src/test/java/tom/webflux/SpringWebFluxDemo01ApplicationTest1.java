package tom.webflux;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import tom.webflux.model.User;

@RunWith(SpringRunner.class)
//启动一个web应用不是mock
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class SpringWebFluxDemo01ApplicationTest1 {
    /**
     * web测试客户端
     */
    @Autowired
    private TestRestTemplate template;

    @BeforeEach
    void setUp() {
    }

    @Test
    public void testFindOne() {
        ResponseEntity<User> user = this.template.getForEntity("/user/1", User.class);
        Assert.assertTrue(user.getStatusCode().is2xxSuccessful());
        Assert.assertEquals(1L, user.getBody().getId().longValue());
        Assert.assertEquals("tom", user.getBody().getName());
    }

    @Test
    public void testAddUser() {
        ResponseEntity<User> resp = this.template.postForEntity("/user", new User(5L, "bulk"), User.class);
        Assert.assertTrue(resp.getStatusCode().is2xxSuccessful());
    }
}