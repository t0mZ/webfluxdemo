package tom.webflux;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import tom.webflux.model.User;

@RunWith(SpringRunner.class)
//启动一个web应用不是mock
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class SpringWebFluxDemo01ApplicationTest2 {

    /**
     * webFlux 测试客户端
     */
    @Autowired
    private WebTestClient client;

    @BeforeEach
    void setUp() {
    }

    @Test
    public void testFindOne() {
        this.client.get().uri("/user/1").accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().is2xxSuccessful()
                .expectBody(User.class)
                .value(m -> System.out.println(m));
    }

    @Test
    public void testAddUser() {
        this.client.post().uri("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(new User(4L, "bull")))
                .exchange()
                .expectStatus().isCreated();
    }

}