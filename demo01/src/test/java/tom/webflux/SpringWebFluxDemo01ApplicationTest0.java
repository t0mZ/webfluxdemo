package tom.webflux;

import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.reactive.function.BodyExtractors;
import org.springframework.web.reactive.function.client.WebClient;

import java.lang.reflect.Type;
import java.util.Objects;

/**
 * 使用自定义WebClient请求，主要在于学会使用WebClient对应Web中的RestTemplate
 */
class SpringWebFluxDemo01ApplicationTest0 {

    @Test
    public void test01() {
        WebClient client = WebClient.builder().baseUrl("http://localhost:8080").build();
        //一秒一个要通过debug日志查看
        client.get().uri("/time")
                //接受类型为text event stream
                .accept(MediaType.TEXT_EVENT_STREAM)
                //是exchange的快捷版，获取内容并且解码
                .retrieve()
                //消息体映射为String, 是连续多个使用Flux
                .bodyToFlux(String.class)
                //打印
                .log()
                //只取10个就中止
                .take(10)
                .doOnNext(System.out::println)
                //阻塞一直到取满10个
                .blockLast();
    }

    @Test
    public void test02() {
        WebClient client = WebClient.builder().baseUrl("http://localhost:8080").build();
        //一秒一个要通过debug日志查看
        client.get().uri("/time")
                //接受类型为text event stream
                .accept(MediaType.TEXT_EVENT_STREAM)
                //获取内容
                .exchange()
                //内容转换
                .flatMapMany(resp -> resp.body(BodyExtractors.toFlux(new ParameterizedTypeReference<ServerSentEvent<String>>() {
                })))
                //过滤null值
                .filter(sse -> Objects.nonNull(sse.data()))
                //打印
                .log()
                //缓冲10个
                .buffer(10)
                //当缓冲到10时，执行next,打印list包含10个event
                .doOnNext(System.out::println)
                //阻塞一直取
                .blockLast();
    }

}