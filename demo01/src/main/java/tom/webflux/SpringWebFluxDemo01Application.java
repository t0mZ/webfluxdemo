package tom.webflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.config.EnableWebFlux;

/**
 * @author ZHUFEIFEI
 */
@EnableWebFlux
@SpringBootApplication
public class SpringWebFluxDemo01Application {
    public static void main(String[] args) {
        SpringApplication.run(SpringWebFluxDemo01Application.class, args);
    }
}
