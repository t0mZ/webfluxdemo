package tom.webflux.route;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import tom.webflux.handler.UserHandler;

/**
 * 配置映射路径和处理器
 * @author ZHUFEIFEI
 */
@Configuration
public class RouteConfig {

    @Bean
    public RouterFunction<ServerResponse> userRouter(UserHandler handler) {
        return RouterFunctions.route(
                RequestPredicates.GET("/user/{id}").and(RequestPredicates.accept(MediaType.APPLICATION_JSON))
                , handler::findOne)

                .andRoute(RequestPredicates.GET("/users").and(RequestPredicates.accept(MediaType.APPLICATION_JSON))
                , handler::findAll)

                .andRoute(RequestPredicates.POST("/user").and(RequestPredicates.accept(MediaType.APPLICATION_JSON))
                , handler::addUser)

                .andRoute(RequestPredicates.DELETE("/user/{id}").and(RequestPredicates.accept(MediaType.APPLICATION_JSON))
                , handler::removeUser)

                .andRoute(RequestPredicates.GET("/time"), handler::tick)
                ;
    }
}
