package tom.webflux;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import tom.webflux.model.User;
import tom.webflux.service.UserService;

/**
 * WebFluxTest 会自动扫描所有controller，具体查看WebFluxTest注释，
 * 不会扫描service，component，repository,所以controller需要用到的
 * bean要通过import导入
 */
@WebFluxTest
@Import({UserService.class})
@RunWith(SpringRunner.class)
class SpringWebFluxDemo02ApplicationTest {

    /**
     * webFlux 测试客户端
     */
    @Autowired
    private WebTestClient client;

    @BeforeEach
    void setUp() {
    }

    @Test
    public void testFindOne() {
        this.client.get().uri("/user/1").accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().is2xxSuccessful()
                .expectBody(User.class)
                .value(m -> System.out.println(m));
    }

    @Test
    public void testAddUser() {
        this.client.post().uri("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(new User(4L, "bull")))
                .exchange()
                .expectStatus().isCreated();
    }

}