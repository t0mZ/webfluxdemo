package tom.webflux.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tom.webflux.model.User;
import tom.webflux.service.UserService;

/**
 * @author ZHUFEIFEI
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/user/{id}")
    public Mono<User> findOne(@PathVariable("id") Long id) {
        //
        return this.userService.findById(id);
    }

    @GetMapping("/users")
    public Flux<User> findAll() {
        //with response body
        return this.userService.findAll();
    }

    @PostMapping("/user")
    public Mono<ResponseEntity<Void>> addUser(@RequestBody User user) {
        this.userService.create(user);
        return Mono.just(ResponseEntity.status(HttpStatus.CREATED).build());
    }

    @DeleteMapping("/user/{id}")
    public Mono<ResponseEntity<Void>> removeUser(@PathVariable("id") Long id) {
        this.userService.delete(id);
        return Mono.just(ResponseEntity.ok().build());
    }

}
