package tom.webflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.web.reactive.config.EnableWebFlux;

/**
 * 采用controller注解形式的web flux
 * @author ZHUFEIFEI
 */
@EnableWebFlux
@SpringBootApplication
public class SpringWebFluxDemo02Application {
    public static void main(String[] args) {
        SpringApplication.run(SpringWebFluxDemo02Application.class, args);
    }
}
