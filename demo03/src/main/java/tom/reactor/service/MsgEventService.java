package tom.reactor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tom.reactor.dao.MsgEventDao;
import tom.reactor.model.MsgEvent;

/**
 * @author ZHUFEIFEI
 */
@Service
public class MsgEventService {

    @Autowired
    private MsgEventDao dao;

    public Mono<Void> saveEvents(Flux<MsgEvent> events) {
        //无返回内容
        return this.dao.saveAll(events).then();
    }


    public Flux<MsgEvent> findEvents() {
        return this.dao.findBy();
    }
}
