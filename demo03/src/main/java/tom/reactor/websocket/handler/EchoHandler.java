package tom.reactor.websocket.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Mono;

/**
 * spring-webflux提供了对websocket的支持
 * @author ZHUFEIFEI
 */
@Slf4j
@Component
public class EchoHandler implements WebSocketHandler {
    /**
     *
     * @param session
     * @return
     */
    @Override
    public Mono<Void> handle(WebSocketSession session) {
        //把收到的消息前面加一个echo发送回去
        return session.send(
                session.receive().map(msg -> session.textMessage("echo ==> " + msg.getPayloadAsText()))
        ).log();
    }
}
