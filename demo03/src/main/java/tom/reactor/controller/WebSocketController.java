package tom.reactor.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author ZHUFEIFEI
 */
@Controller
public class WebSocketController {

    @GetMapping("/websocket")
    public String webSocket(final Model model) {
        model.addAttribute("title", "webflux support websocket test");
        return "websocket";
    }
}
