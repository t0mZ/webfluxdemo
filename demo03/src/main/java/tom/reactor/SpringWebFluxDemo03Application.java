package tom.reactor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.config.EnableWebFlux;

/**
 * 采用controller注解形式的web flux
 * @author ZHUFEIFEI
 */
@EnableWebFlux
@SpringBootApplication
public class SpringWebFluxDemo03Application {
    public static void main(String[] args) {
        SpringApplication.run(SpringWebFluxDemo03Application.class, args);
    }
}
