package tom.reactor.dao;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;
import tom.reactor.model.User;

/**
 * reactor dao
 * @author ZHUFEIFEI
 */
public interface UserDao extends ReactiveCrudRepository<User, Long>{

    /**
     * @param name
     * @return
     */
    Mono<User> findByName(String name);
}
