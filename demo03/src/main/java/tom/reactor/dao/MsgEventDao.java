package tom.reactor.dao;

import org.springframework.data.mongodb.repository.Tailable;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import tom.reactor.model.MsgEvent;

/**
 * @author ZHUFEIFEI
 */
public interface MsgEventDao extends ReactiveCrudRepository<MsgEvent, Long> {

    /**
     * 相当于tailf，无限的获取document中的数据直到document被删除, 前提条件该document必须有限制大小
     * 因为findAll在父类实现所以这里只能取名字findBy
     * @return
     */
    @Tailable
    Flux<MsgEvent> findBy();
}
