package tom.reactor.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author ZHUFEIFEI
 */
@Data
@Document
@NoArgsConstructor
@AllArgsConstructor
public class MsgEvent {
    @Indexed
    private Long id;
    private String msg;
}