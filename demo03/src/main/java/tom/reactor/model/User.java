package tom.reactor.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author ZHUFEIFEI
 */
@Data
//mongodb注解
@Document
public class User {
    @Id
    private Long id;
    @Indexed(unique = true)
    private String name;

    private String address;

    public User() {
    }

    public User(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public User(Long id, String name, String address) {
        this(id, name);
        this.address = address;
    }
}
