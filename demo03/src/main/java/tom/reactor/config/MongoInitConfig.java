package tom.reactor.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import tom.reactor.model.MsgEvent;

/**
 * 初始化
 * @author ZHUFEIFEI
 */
@Configuration
public class MongoInitConfig {

    /**
     * SpringBoot 启动会扫描所有CommandLineRunner的实例并执行run方法
     * @param mongo
     * @return
     */
    @Bean
    public CommandLineRunner initData(MongoOperations mongo) {
        return (String... args) -> {
            //如果document已存在先删除，正式环境注意不要随便使用
            mongo.dropCollection(MsgEvent.class);
            //创建一个指定大小的集合。size 是指的字节， 限制100kb,500条数据
            mongo.createCollection(MsgEvent.class, CollectionOptions.empty().maxDocuments(500).size(1024 * 100).capped());
        };
    }
}
