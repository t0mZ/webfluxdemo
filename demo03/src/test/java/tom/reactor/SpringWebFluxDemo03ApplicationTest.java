package tom.reactor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import tom.reactor.model.User;

import java.util.List;

/**
 * 可以把WebTestClient 看作testRestTemplate
 *  WebClient 看作 RestTemplate
 */
class SpringWebFluxDemo03ApplicationTest {

    private WebTestClient client;

    @BeforeEach
    public void setUp() {
        client = WebTestClient.bindToServer().baseUrl("http://localhost:8080").build();
    }

    @Test
    public void testFindAllWithInterval() throws InterruptedException {
        //一秒返回一个的，数据类型是User, debug可以看出一秒接收到一个数据
        this.client.get().uri("/user/per")
                //请内容类型
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .exchange()
                .expectStatus().is2xxSuccessful()
                //一秒返回一个，并且返回的数据是多个，期望返回为list
                .expectBodyList(User.class)
                .value(System.out::println)
                ;
    }

    @Test
    public void testFindAll() {
        //一次性全部返回的是List
        client.get().uri("/user")
                .exchange()
                .expectStatus().is2xxSuccessful()
                .expectBody(List.class)
                .value(System.out::println)
        ;
    }

    @Test
    public void testFindOne() {
        client.get().uri("/user/1")
                .exchange()
                .expectStatus().is2xxSuccessful()
                .expectBody(User.class);
    }

    @Test
    public void testAddUser() {
        for (int i=1; i <= 10; i++) {
            this.client.post().uri("/user")
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(BodyInserters.fromObject(new User(Long.valueOf(i), "tom"+i, "beijing"+i)))
                    .exchange()
            .expectStatus().is2xxSuccessful()
            ;
        }
    }

}